﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class AndroidData : MonoBehaviour {

    private float _height = 0.0f;
    public Text HeightValue;
    private string _action = "stop";

    private void Update()
    {
        HeightValue.text = _height.ToString("0.0");

        if(_action == "up" && _height <= 120.0f)
        {
            _height += Time.deltaTime *2;
        }
        else if(_action == "down" && _height >= 0.0f)
        {
            _height -= Time.deltaTime*2;
        }
        else
        {
            _action = "stop";
        }

        if (UDPData.Flag)
        {
            UDPData.SendString("[$]tracking,[$$]Android,[$$$]Lift,height," + _height.ToString("0.0") + "," + "0" + "," + "0" + "," + "0" + ";");
        }

        
    }

    public void UpdateLiftHeight(string action)
    {
        if(_action == action)
        {
            _action = "stop";
        }
        else
        {
            _action = action;
        }
            
    }
}
