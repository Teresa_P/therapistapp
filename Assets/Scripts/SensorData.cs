﻿using UnityEngine;
using System.Collections;

public class SensorData : MonoBehaviour {

    public string url = "http://localhost/hr/hr.txt";
    WWW www;

    void Start ()
    {
        
        
    }

    void Update()
    {
        www = new WWW(url);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
        }
        else {
            Debug.Log("WWW Error: " + www.error);
        }
    }
}
