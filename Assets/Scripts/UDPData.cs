using UnityEngine;
using System.Collections;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System;
using UnityEngine.UI;

public class UDPData : MonoBehaviour {
    
    public InputField AdressInputField, PortInputField;
    public Toggle Connect;
    private bool _first = true;

    private string _iP;
    private int _port;

    private string ipField = "192.168.0.100";
	private string portField = "1204";
	
	public static bool Flag;
	
    public static IPEndPoint RemoteEndPoint;
    public static UdpClient Client;

    private static bool _isConnected;

	void Start ()
	{
        AdressInputField.text = ipField;
        PortInputField.text = portField;
	}

	void Update () 
	{
        ipField = AdressInputField.text;

        portField = PortInputField.text;
        
        if (Connect.isOn)
        {
            Debug.Log("Is on");
            if (_first)
            {
                Debug.Log("Start UDP");
                _iP = ipField;
                _port = int.Parse(portField);
                Init();
                Flag = true;
                Handheld.Vibrate();
                _first = false;
            }
        }
        else
        {
            if (Flag)
            {
                if (!_first)
                {
                    Flag = false;
                    _first = true;
                    Client.Close();
                    Debug.Log("Stop UDP");
                    Handheld.Vibrate();
                }
            }
        }
	}
	
	private void Init()
    { 
        RemoteEndPoint = new IPEndPoint(IPAddress.Parse(_iP), _port);
        Client = new UdpClient();
        _isConnected = true;
        Debug.Log("Connected");
    }

    
    public static void SendString(string message)
    {
        if (_isConnected)
        { 
            try
            {
                if (message != "")
                {
                    // UTF8 encoding to binary format.
                    byte[] data = Encoding.UTF8.GetBytes(message);
                    print(message);
                    // Send the message to the remote client.
                    Client.Send(data, data.Length, RemoteEndPoint);
                }
            }

            catch (Exception err)
            {
                Debug.Log(err.ToString());
            }
        }
    }


    void OnApplicationQuit()
    {
        if (Client != null)
        {
            _isConnected = false;
            Client.Close();
        }
    }	
}
